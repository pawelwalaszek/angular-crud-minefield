(function () {

	app.controller('UserListController', function UserListController($scope, $location, UserService) {

		$scope.createNewUser = function () {
			$location.path('/user/add');
		};

		$scope.editUser = function (userId) {
			$location.path('/user/edit/' + userId);
		};

		$scope.deleteUser = function (userId) {

			// 1 way

//			UserService.delete({id: userId}, function () {
//				$scope.users = UserService.query();
//			});

			// 2 way - promise

			UserService.delete({id: userId}).$promise.then(function () {
				$scope.users = UserService.query();
			});
		};

		$scope.users = UserService.query();
    });

	app.controller('UserDetailCreationController', function UserDetailCreationController($scope, $location, UserService) {

		$scope.createNewUser = function () {
			if($scope.userAddForm.$valid) {
				UserService.save($scope.user, function () {
					$location.path('/user/list');
				});
			}
		};

		$scope.cancel = function () {
			$location.path('/user/list');
		};
	});

	app.controller('UserDetailEditController', function UserDetailEditController($scope, $location, $routeParams, UserService) {

		$scope.updateUser = function () {
			if($scope.userEditForm.$valid) {
				UserService.update($scope.user, function () {
					$location.path('/user/list');
				});
			}
		};

		$scope.cancel = function () {
			$location.path('/user/list');
		};

		$scope.user = UserService.get({id: $routeParams.user_id});
	});

}());