package me.pjw.crud.domain;

public enum Gender {
	MALE, FEMALE
}
