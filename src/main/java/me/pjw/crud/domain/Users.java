package me.pjw.crud.domain;

import com.google.common.base.Predicate;

public class Users {

	public static Predicate<User> hasLogin(String login) {
		return new Predicate<User>() {
			@Override
			public boolean apply(User u) {
				return u.getLogin().equals(login);
			}
		};
	}

	public static Predicate<User> hasEmail(String email) {
		return new Predicate<User>() {
			@Override
			public boolean apply(User u) {
				return u.getEmail().equals(email);
			}
		};
	}
}
