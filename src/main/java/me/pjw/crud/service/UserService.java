package me.pjw.crud.service;

import java.util.List;

import me.pjw.crud.db.UserRepository;
import me.pjw.crud.domain.User;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.google.common.base.Optional;
import com.google.common.base.Predicate;
import com.google.common.collect.Iterables;

@Service
public class UserService {

	@Autowired
	private UserRepository userRepository;

	public User getUserById(Long id) {
		return userRepository.getUserById(id);
	}

	public List<User> getUsers() {
		return userRepository.getUsers();
	}

	public void createUser(User user) {
		userRepository.insert(user);
	}

	public void updateUser(User user) {
		userRepository.update(user);
	}

	public void deleteUser(Long id) {
		userRepository.delete(id);
	}

	public boolean checkAvailability(Predicate<User> predicate) {
		List<User> users = getUsers();
		if(users == null || users.size() == 0) {
			return true;
		}
		Optional<User> result = Iterables.tryFind(users, predicate);
		return !result.isPresent();
	}
}
