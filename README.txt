To run:

mvn spring-boot:run

RESTful API

To create:

curl -H 'Content-Type: application/json' -X POST -d '{"id":100,"login":"ania","password":"ania"}' localhost:8080/users

To update:

curl -H 'Content-Type: application/json' -X PUT -d '{"id":100,"login":"ania","password":"ania"}' localhost:8080/users/100

To delete:

curl -X DELETE localhost:8080/users/100

To read:

curl localhost:8080/users/100

To read all list:

curl localhost:8080/users

To check login or e-mail availability:

curl localhost:8080/check/login/lmessi
curl localhost:8080/checl/email/l.messi@fcbarca.com
